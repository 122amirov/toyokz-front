import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    oem: []
  },
  getters: {
    getOem: state => {
      return state.oem;
    }
  },
  mutations: {
    ADD_OEM: (state, payload) => {
      state.oem.push(payload);
    }
  }
})