import Vue from 'vue'
import VueRouter from 'vue-router'

const Home = () => import('../components/Home')
const Phone = () => import('../components/Phone')
const ListDetails = () => import('../components/ListDetails')
const OEM = () => import('../components/ListDetailsOEM.vue')
const Prices = () => import('../components/Prices.vue')
const Cart = () => import('../components/Cart.vue')
const Pay = () => import('../components/Pay.vue')
const Main = () => import('../components/Main.vue')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main
  },
  {
    path: '/vin',
    name: 'Home',
    component: Home
  },
  {
    path: '/phone',
    name: 'Phone',
    component: Phone
  },
  {
    path: '/list-details',
    name: 'ListDetails',
    component: ListDetails
  },
  {
    path: '/oem',
    name: 'ListDetailsOEM',
    component: OEM
  },
  {
    path: '/prices',
    name: 'Prices',
    component: Prices
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart
  },
  {
    path: '/pay',
    name: 'Pay',
    component: Pay
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
